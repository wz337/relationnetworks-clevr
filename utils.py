import json
import os
import pickle
import re

import torch
from tqdm import tqdm
import config

classes = {
            'number':['0','1','2','3','4','5','6','7','8','9','10'],
            'material':['rubber','metal'],
            'color':['cyan','blue','yellow','purple','red','green','gray','brown'],
            'shape':['sphere','cube','cylinder'],
            'size':['large','small'],
            'exist':['yes','no']
        }


def compute_class(answer):
    for key, values in classes.items():
        if answer in values:
            return key

    raise ValueError('Answer {} does not belong to a known class'.format(answer))

def build_dictionaries(clevr_dir):
    cached_dictionaries = os.path.join(clevr_dir, 'questions', 'CLEVR_build_dictionaries.pkl')
    if os.path.exists(cached_dictionaries):
        print('==> using cached dictionaries:{}'.format(cached_dictionaries))
        with open (cached_dictionaries, 'rb') as f:
            return pickle.load(f)

    quest_to_idx = {}
    answer_to_idx = {}
    answer_idx_to_class = {}
    json_train_filename = os.path.join(clevr_dir, 'questions', 'CLEVR_train_questions.json')

    #load all words from training data
    with open(json_train_filename, "r") as f:
        questions = json.load(f)['questions']
        for q in tqdm(questions):
            question = tokenize(q['question'])
            answer = q['answer']
            for word in question:
                if word not in quest_to_idx:
                    #one-based indexing
                    #zero is reserved for padding
                    quest_to_idx[word] = len(quest_to_idx) + 1

            a = answer.lower()
            if a not in answer_to_idx:
                idx = len(answer_to_idx) + 1
                answer_to_idx[a] = idx
                answer_idx_to_class[idx] = compute_class(a)

    res = (quest_to_idx, answer_to_idx, answer_idx_to_class)
    with open(cached_dictionaries, 'wb') as f:
        pickle.dump(res, f)

    return res

def to_dictionary_indexes(dictionary, sentence):
    """
    Outputs indexes of dictionary corresponding to the words in the sequence. (case-insensitive)
    :param dictionary: which dictionary
    :param sentence:
    :return:
    """
    split = tokenize(sentence)
    idxs = torch.LongTensor([dictionary[w] for w in split])
    return idxs

def collate_samples_image(batch):
    return collate_samples(batch, False)

def collate_samples_state_description(batch):
    return collate_samples(batch, True)

def collate_samples(batch, state_description):
    """
    Used by DatasetLoader to merge together multiple samples into one mini-batch
    :param batch:
    :param state_description:
    :return:
    """
    images = [d['image'] for d in batch]
    questions = [d['question'] for d in batch]
    answers = [d['answer'] for d in batch]

    #questions are padded to a fixed length (the maximum length in questions)
    #so they can be inserted in a tensor
    batch_size = len(batch)
    max_len = max(map(len, questions))

    padded_questions = torch.LongTensor(batch_size, max_len).zero_()
    for i, q in enumerate(questions):
        padded_questions[i, :len(q)] = q

    collate_batch = dict(
        image=torch.stack(images),
        answer=torch.stack(answers),
        question=torch.stack(padded_questions)
    )

    return collate_batch


def tokenize(sentence):
    s = re.sub('([.,;:!?()])', r' \1 ', sentence)
    s = re.sub('\s{2,}', ' ', s)

    return [w.lower() for w in s.split()]


def load_tensor_data(data_batch, cuda, invert_questions, volatile=False):
    # prepare input
    var_kwargs = dict(volatile=True) if volatile else dict(requires_grad=False)

    qst = data_batch['question']
    if invert_questions:
        # invert question indexes in this batch
        qst_len = qst.size()[1]
        qst = qst.index_select(1, torch.arange(qst_len - 1, -1, -1).long())

    img = torch.autograd.Variable(data_batch['image'], **var_kwargs)
    qst = torch.autograd.Variable(qst, **var_kwargs)
    label = torch.autograd.Variable(data_batch['answer'], **var_kwargs)
    if cuda:
        img, qst, label = img.cuda(), qst.cuda(), label.cuda()

    label = (label - 1).squeeze(1)
    return img, qst, label



