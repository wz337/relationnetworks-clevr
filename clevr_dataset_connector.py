import json
import os
import pickle
from PIL import Image

from collections import Counter
from torch.utils.data import Dataset

import utils
import torch
import cv2
import numpy as np

class ClevrDatasetImages(Dataset):
    """
    Loads image from the CLEVR dataset
    """

    def __init__(self, clevr_dir, split, transform=None):
        """
        :param clevr_dir: Root directory of CLEVR dataset
        :param split: Specifies which sub-dataset we want to read, train, test, or val
        :param transform: Optional transform to be applied on a sample
        """
        self.img_dir = os.path.join(clevr_dir, 'images', split)
        self.split = split
        self.transform = transform

    def __len__(self):
        return len(os.listdir(self.img_dir))

    def __getitem__(self, idx):
        padded_index = str(idx).rjust(6, '0')
        img_filename = os.path.join(self.img_dir, 'CLEVR_'+ self.split + '_{}.png'.format(padded_index))
        image = Image.open(img_filename).convert('RGB')

        if self.transform:
            image = self.transform

        return image

# c = ClevrDatasetImages('/Users/iriszhang/CornellTech/CLEVR-Experiment/CLEVR_v1.0', 'test')
# img = (c.__getitem__(1))
# cv2.imshow('sample', np.array(img))
# cv2.waitKey(0)

class ClevrDataset(Dataset):
    def __init__(self, clevr_dir, train, dictionaries, transform=None):
        """
        :param clevr_dir: root directory
        :param split: Specifies which sub-dataset we want to read, train, test, or val
        :param dictionaries: ***
        :param transform: Optional transform to be applied on a sample
        """
        if train:
            quest_json_filename = os.path.join(clevr_dir, 'questions', 'CLEVR_train_questions.json')
            self.img_dir = os.path.join(clevr_dir, 'images', 'train')
        else:
            quest_json_filename = os.path.join(clevr_dir, 'questions', 'CLEVR_val_questions.json')
            self.img_dir = os.path.join(clevr_dir, 'images', 'val')

        cached_questions = quest_json_filename.replace('.json', '.pkl')
        if os.path.exists(cached_questions):
            print('==>using cached questions: {}'.format(cached_questions))
            with open(cached_questions, 'rb') as f:
                self.questions = pickle.load(f)
        else:
            with open(quest_json_filename, 'r') as json_file:
                self.questions = json.load(json_file)['questions']
            with open(cached_questions, 'wb') as f:
                pickle.dump(self.questions, f)

        self.clevr_dir = clevr_dir
        self.transform = transform
        self.dictionaries = dictionaries

    def answer_weights(self):
        n = float(len(self.questions))
        answer_count = Counter(q['answer'].lower() for q in self.questions)
        weights = [n/answer_count[q['answer'].lower()] for q in self.questions]
        return weights

    def __len__(self):
        return len(self.questions)

    def __getitem__(self,idx):
        current_question = self.questions[idx]
        img_filename = os.path.join(self.img_dir, current_question['image_filename'])
        image = Image.open(img_filename).convert('RGB')

        question = utils.to_dictionary_indexes(self.dictionaries[0], current_question['question'])
        answer = utils.to_dictionary_indexes(self.dictionaries[1], current_question['answer'])

        sample = {'image': image, 'question': question, 'answer': answer}

        if self.transform:
            sample['image'] = self.transform(sample['image'])

        return sample



